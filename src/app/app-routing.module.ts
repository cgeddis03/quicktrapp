import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeadsComponent } from './leads/leads.component';
import { LeadComponent } from './lead/lead.component';
import { LeadAddEditComponent } from './lead-add-edit/lead-add-edit.component';

const routes: Routes = [
  { path: '', component: LeadsComponent, pathMatch: 'full' },
  { path: 'lead/:id', component: LeadComponent },
  { path: 'add', component: LeadAddEditComponent },
  { path: 'lead/edit/:id', component: LeadAddEditComponent },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
