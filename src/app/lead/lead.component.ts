import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { LeadService } from '../lead.service';
import { Lead } from '../models/Lead';


@Component({
  selector: 'app-lead',
  templateUrl: './lead.component.html',
  styleUrls: ['./lead.component.css']
})
export class LeadComponent implements OnInit {
  lead$: Observable<Lead>;
  id: number;
  constructor(private leadService: LeadService, private avRoute: ActivatedRoute) { 
    const idParam = 'id';
    if(this.avRoute.snapshot.params[idParam]){
      this.id = this.avRoute.snapshot.params[idParam];
    }
  }

  ngOnInit(): void {
    this.loadLead();
  }

  loadLead(){
    this.lead$ = this.leadService.getLead(this.id);
  }

}