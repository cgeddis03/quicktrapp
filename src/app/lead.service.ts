import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Lead } from './models/Lead';


@Injectable({
  providedIn: 'root'
})
export class LeadService {
  myApiUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json; charset=utf-8'
    })
  };
  constructor(private http: HttpClient) {
    this.myApiUrl = "URL GOES HERE";
   }

  getLeads(): Observable<Lead[]> {
    return this.http.get<Lead[]>(this.myApiUrl)
    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );
  }

  getLead(id: number): Observable<Lead> {
    return this.http.get<Lead>(this.myApiUrl + id)
    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );
  }

  saveLead(Lead): Observable<Lead> {
    return this.http.post<Lead>(this.myApiUrl, JSON.stringify(Lead), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );
  }

  updateLead(id: number, Lead): Observable<Lead> {
    return this.http.put<Lead>(this.myApiUrl + id, JSON.stringify(Lead), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );
  }

  deleteLead(id: number): Observable<Lead> {
    return this.http.delete<Lead>(this.myApiUrl + id)
    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );
  }

  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
  
}