import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LeadsComponent } from './leads/leads.component';
import { LeadComponent } from './lead/lead.component';
import { LeadAddEditComponent } from './lead-add-edit/lead-add-edit.component';
import { LeadService } from './lead.service';

@NgModule({
  declarations: [
    AppComponent,
    LeadsComponent,
    LeadComponent,
    LeadAddEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    LeadService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
