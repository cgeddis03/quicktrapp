import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LeadService } from '../lead.service';
import { Lead } from '../models/Lead';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.css']
})
export class LeadsComponent implements OnInit {
  leads$: Observable<Lead[]>;
  constructor(private LeadService: LeadService) { 
  }

  ngOnInit(): void {
    this.loadLeads();
  }

  loadLeads(){
    this.leads$ = this.LeadService.getLeads();
  }

  delete(id) {
    const ans = confirm('Do you want to delete lead with id: ' + id);
    if (ans) {
      this.LeadService.deleteLead(id).subscribe((data) => {
        this.loadLeads();
      });
    }
  }

}